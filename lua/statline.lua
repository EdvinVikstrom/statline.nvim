local StatLine = {}

local DefaultTheme = require("statline.themes.default")

---@param conf  table with options {option = value, ...}. |statline-config|
---@returns StatLine metatable
function StatLine:new(conf)
  local self = setmetatable({}, { __index = StatLine})
  self.config = conf

  self:setup_config()
  return self
end

function StatLine:setup_config()
  local conf = self.config
  conf.status_lines = (conf.status_lines or {})
  conf.tab_lines = (conf.tab_lines or {})
  if (conf.enable_status_line == nil) then conf.enable_status_line = true end
  if (conf.enable_tab_line == nil) then conf.enable_tab_line = false end

  if (conf.enable_status_line) then
    self:enable_status_line()
  end

  if (conf.enable_tab_line) then
    self:enable_tab_line()
  end
end

---@param theme  Theme metatable
function StatLine:load_theme(theme)
  self.theme = theme
  self.theme:setup_highlight()
  self.theme:setup_parts()
  self.theme:setup_lines()
end

function StatLine:get_status_line()
  local win = vim.g.statusline_winid
  return self.theme:render_status_line(win)
end

function StatLine:get_tab_line()
  local tab = vim.fn.tabpagenr()
  return self.theme:render_tab_line(tab)
end

---@param line  Line metatable
function StatLine:add_status_line(line)
  table.insert(self.config.status_lines, line)
end

---@param line  Line metatable
function StatLine:add_tab_line(line)
  table.insert(self.config.tab_lines, line)
end

function StatLine:enable_status_line()
  vim.api.nvim_set_option("statusline", "%!luaeval('_G.statline:get_status_line()')")
end

function StatLine:disable_status_line()
  vim.api.nvim_set_option("statusline", "")
end

function StatLine:enable_tab_line()
  vim.cmd("set showtabline=2")
  vim.api.nvim_set_option("tabline", "%!luaeval('_G.statline:get_tab_line()')")
end

function StatLine:disable_tab_line()
  vim.cmd("set showtabline=1")
  vim.api.nvim_set_option("tabline", "")
end

---@param conf  table with options {option = value, ...}. |statline-config|
function StatLine:setup(conf)
  _G.statline = StatLine:new(conf)

  local theme = DefaultTheme:new(_G.statline.config)
  _G.statline:load_theme(theme)
end

return StatLine
