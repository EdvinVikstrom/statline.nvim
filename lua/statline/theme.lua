local Theme = {}

---@returns Theme metatable
function Theme:inherit()
  local self = setmetatable({}, { __index = Theme })
  return self
end

---@param name  theme name
---@returns Theme metatable
function Theme:new(name)
  local self = setmetatable({}, { __index = Theme })
  self.name = name
  self.status_lines = {}
  self.tab_lines = {}
  return self
end

function Theme:setup_highlight()
end

function Theme:setup_parts()
end

function Theme:setup_lines()
end

function Theme:render_status_line(win)
end

function Theme:render_tab_line()
end

---@param line  Line metatable
function Theme:add_status_line(line)
  table.insert(self.status_lines, line)
end

---@param line  Line metatable
function Theme:add_tab_line(line)
  table.insert(self.tab_lines, line)
end

return Theme
