local Help = {}

---@param mode  mode from vim.fn.mode() or vim.api.nvim_get_mode()
---@returns mode name, short mode name
function Help:get_mode_name(mode)
  local cv = string.char(22) -- CTRL-V
  local cs = string.char(19) -- CTRL-S
  if (mode == "n") then 		return "NORMAL", "n"
  elseif (mode == "no") then 		return "NORMAL", "n"
  elseif (mode == "nov") then 		return "NORMAL", "n"
  elseif (mode == "noV") then 		return "NORMAL", "n"
  elseif (mode == "no"..cv) then 	return "NORMAL", "n"

  elseif (mode == "niI") then 		return "INSERT (normal)", "i"
  elseif (mode == "niR") then 		return "REPLACE (normal)", "r"
  elseif (mode == "niV") then 		return "VISUAL (normal)", "v"
  elseif (mode == "nt") then 		return "NORMAL (terminal)", "n"

  elseif (mode == "v") then 		return "VISUAL", "v"
  elseif (mode == "vs") then 		return "VISUAL", "v"
  elseif (mode == "V") then 		return "V-LINE", "v"
  elseif (mode == "Vs") then 		return "V-LINE", "v"
  elseif (mode == cv) then 		return "V-BLOCK", "v"
  elseif (mode == cs.."s") then 	return "V-BLOCK", "v"
  elseif (mode == "s") then 		return "SELECT", "s"
  elseif (mode == "S") then 		return "S-LINE", "s"
  elseif (mode == "S") then 		return "S-BLOCK", "s"
  elseif (mode == "i") then 		return "INSERT", "i"
  elseif (mode == "ic") then 		return "INSERT", "i"
  elseif (mode == "ix") then 		return "INSERT", "i"
  elseif (mode == "R") then 		return "REPLACE", "r"
  elseif (mode == "Rc") then 		return "REPLACE", "r"
  elseif (mode == "Rx") then 		return "REPLACE", "r"
  elseif (mode == "Rv") then 		return "V-REPLACE", "r"
  elseif (mode == "Rvc") then 		return "V-REPLACE", "r"
  elseif (mode == "Rvx") then 		return "V-REPLACE", "r"
  elseif (mode == "c") then 		return "COMMAND", "c"
  elseif (mode == "cv") then 		return "COMMAND", "c"
  elseif (mode == "r") then 		return "CONFIRM", "?"
  elseif (mode == "rm") then 		return "MORE", "m"
  elseif (mode == "r?") then 		return "CONFIRM", "?"
  elseif (mode == "!") then 		return "SHELL", ">"
  elseif (mode == "t") then 		return "TERMINAL", "t"
  end
  return "???"
end

---@param wins  table of win
function Help:redraw_status_lines(wins)
  for i, win in ipairs(wins) do
    local opt = vim.api.nvim_win_get_option(win, "number")
    vim.api.nvim_win_set_option(win, "number", opt)
  end
end

return Help
