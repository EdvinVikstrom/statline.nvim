local Part = {}

---@param render_cb  render callback(conf, builder)
---@returns Part metatable
function Part:new(render_cb)
  local self = setmetatable({}, { __index = Part })
  self.render_cb = render_cb
  return self
end

---@param conf  table with options
---@param builder  Builder metatable
function Part:render(conf, builder)
  self.render_cb(conf, builder)
end

return Part
