local Line = {}

local Builder = require("statline.builder")

---@param fallback_hl  highlight group
---@is_suitable_cb  suitable callback(conf)
---@returns Line metatable
function Line:new(fallback_hl, is_suitable_cb)
  local self = setmetatable({}, { __index = Line })
  self.fallback_hl = fallback_hl
  self.is_suitable_cb = is_suitable_cb
  self.left_section = {}
  self.center_section = {}
  self.right_section = {}
  return self
end

---@param conf  table with options
---@returns string with status line format
function Line:render(conf)
  local builder = Builder:new(self.fallback_hl)

  for i, part in ipairs(self.left_section) do
    part:render(conf, builder)
  end

  builder:split()
  for i, part in ipairs(self.center_section) do
    part:render(conf, builder)
  end

  builder:split()
  for i, part in ipairs(self.right_section) do
    part:render(conf, builder)
  end

  local build = builder:build(conf.hl_set)
  return ""..build[1].."%="..build[2].."%="..build[3]
end

---@param part  Part metatable
function Line:add_left(part)
  table.insert(self.left_section, part)
end

---@param part  Part metatable
function Line:add_center(part)
  table.insert(self.center_section, part)
end

---@param part  Part metatable
function Line:add_right(part)
  table.insert(self.right_section, part)
end

---@returns true if line is suitable
function Line:is_suitable(conf)
  return self.is_suitable_cb(conf)
end

return Line
