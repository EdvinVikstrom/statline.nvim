local Builder = {}
local Part = {}


-- ### Part ### --

---@returns Part metatable
function Part:inherit()
  local self = setmetatable({}, { __index = Part })
  return self
end

---@param type  part type
---@returns Part metatable
function Part:new(type)
  local self = setmetatable({}, { __index = Part })
  self.type = type
  return self
end


-- ### TextPart ### --

local TextPart = Part:inherit()

---@param hl  highlight group
---@param text  string
---@returns TextPart metatable
function TextPart:new(hl, text)
  local self = setmetatable(Part:new("text"), { __index = TextPart })
  self.hl = hl
  self.text = text
  return self
end

---@param hl_set  HlSet metatable
---@returns string
function TextPart:render(hl_set)
  return "%#"..hl_set:get(self.hl).."#"..self.text
end

---@returns highlight group
function TextPart:get_hl()
  return self.hl
end

---@returns string
function TextPart:get_text()
  return self.text
end


-- ### SepPart ### --

local SepPart = {}

---@param symbol  one character
---@param reverse  render highlight in reverse
function SepPart:new(symbol, reverse)
  local self = setmetatable(Part:new("sep"), { __index = SepPart })
  self.symbol = symbol
  self.reverse = reverse
  return self
end

---@param hl_set  HlSet metatable
---@param prev_hl  previous highlight group or fallback_hl
---@param next_hl  next highlight group or fallback_hl
---@returns string
function SepPart:render(hl_set, prev_hl, next_hl)
  if (self.reverse) then
    return "%#"..hl_set:get(hl_set:blend(next_hl, prev_hl)).."#"..self.symbol
  end
  return "%#"..hl_set:get(hl_set:blend(prev_hl, next_hl)).."#"..self.symbol
end

---@returns symbol character
function SepPart:get_symbol()
  return self.symbol
end

---@returns true if reverse
function SepPart:is_reverse()
  return self.reverse
end


-- ### Builder ### --

---@param fallback_hl  highlight group
function Builder:new(fallback_hl)
  local self = setmetatable({}, { __index = Builder })
  self.fallback_hl = fallback_hl
  self.parts = {}
  return self
end

---@param hl  highlight group to use
---@param text  string
function Builder:write(hl, text)
  table.insert(self.parts, TextPart:new(hl, text))
end

---@param conf  table{separator: character, alt_separator: character, alt_sep_hl: highlight group for alt_separator}
---@param items  table of items
function Builder:tabs(conf, items)
  for i, item in ipairs(items) do
    local item = items[i]
    
    local next_active = false
    if (i ~= #items) then
      next_active = items[i + 1].active
    end

    self:write(item.hl, item.text)
    if (item.active or next_active or i == #items) then
      self:separate(conf.separator, false)
    else
      self:write(conf.alt_sep_hl, conf.alt_separator)
    end
  end
end

---@param symbol  one character
---@param reverse  reverse highlight
function Builder:separate(symbol, reverse)
  table.insert(self.parts, SepPart:new(symbol, reverse))
end

function Builder:split()
  table.insert(self.parts, {type = "split"})
end

---@param hl_set  HlSet metatable
---@returns string
function Builder:build(hl_set)
  local texts = {}
  local text = ""

  local prev_hl = self.fallback_color
  local next_hl = self.fallback_color

  local prev = nil
  for i, item in ipairs(self.parts) do
    local next = self.parts[i + 1]
    if (item.type == "split") then
      table.insert(texts, text)
      text = ""
    else
      if (prev ~= nil and prev.get_hl) then prev_hl = prev:get_hl() end
      if (next ~= nil and next.get_hl) then next_hl = next:get_hl() end
      text = text..item:render(hl_set, prev_hl, next_hl)
      prev = item
    end
  end
  table.insert(texts, text)
  return texts
end

return Builder
