local Theme = require("statline.theme")
local DefaultTheme = Theme:inherit()

local Part = require("statline.part")
local Line = require("statline.line")
local HlSet = require("statline.hlset")
local Help = require("statline.help")

function DefaultTheme:new(conf)
  local self = setmetatable(Theme:new("default"), { __index = DefaultTheme })
  self.config = (conf or {})

  self:setup_config()
  return self
end

function DefaultTheme:setup_config()
  local conf = self.config
  conf.symbols = (conf.symbols or {})
  conf.symbols.right_sep = (conf.symbols.right_sep or "")
  conf.symbols.left_sep = (conf.symbols.left_sep or "")
  conf.symbols.right_alt_sep = (conf.symbols.right_alt_sep or "")
  conf.symbols.left_alt_sep = (conf.symbols.left_alt_sep or "")
end

function DefaultTheme:setup_highlight()
  self.hl = HlSet:new("statline")
  self.hl:add_hl("none_bg", true, nil, "white", nil)
  self.hl:add_hl("color1", true, nil, 245, 237)
  self.hl:add_hl("color1_bold", true, "bold", 245, 237)
  self.hl:add_hl("color2", true, nil, 245, 239)
  self.hl:add_hl("color2_bold", true, "bold", 245, 239)
  self.hl:add_hl("color3", true, nil, 245, 243)
  self.hl:add_hl("color3_bold", true, "bold", 245, 243)
  self.hl:add_hl("gray", true, nil, "black", "gray")
  self.hl:add_hl("gray_bold", true, "bold", "black", "gray")
  self.hl:add_hl("blue", true, nil, "black", "blue")
  self.hl:add_hl("blue_bold", true, "bold", "black", "blue")
  self.hl:add_hl("green", true, nil, "black", "green")
  self.hl:add_hl("green_bold", true, "bold", "black", "green")
  self.hl:add_hl("orange", true, nil, "black", 166)
  self.hl:add_hl("orange_bold", true, "bold", "black", 166)
  self.hl:define_all()
end

function DefaultTheme:setup_parts()
  local symbols = self.config.symbols

  self.empty_part = Part:new(function(conf, builder)
    builder:write("none_bg", "")
  end)

  self.mode_part = Part:new(function(conf, builder)
    builder:write(conf.accent.."_bold", " "..conf.mode_name.." ")
    builder:separate(symbols.right_sep, false)
    builder:write("color2", "")
    builder:separate(symbols.right_sep, false)
  end)

  self.name_part = Part:new(function(conf, builder)
    builder:write("color1", " %F")
  end)

  self.type_part = Part:new(function(conf, builder)
    builder:separate(symbols.left_sep, true)
    local ftype = vim.api.nvim_buf_get_option(conf.buf, "filetype")
    if (ftype ~= nil and ftype ~= "") then
      builder:write("color2", " %Y ")
    else
      builder:write("color2", "")
    end
  end)

  self.pos_part = Part:new(function(conf, builder)
    builder:separate(symbols.left_sep, true)
    builder:write(conf.accent.."_bold", " %p%% ")
    builder:write(conf.accent, "%l/%L ")
  end)

  self.help_part = Part:new(function(conf, builder)
    builder:write("gray_bold", " HELP ")
    builder:separate(symbols.right_sep, false)
    builder:write("color2", "")
    builder:separate(symbols.right_sep, false)
  end)

  self.buffers_part = Part:new(function(conf, builder)
    builder:write("color2", "")
    local bufs = vim.api.nvim_list_bufs()
    local listed = {}

    local should_list = function(buf)
      return vim.api.nvim_buf_get_option(buf, "buflisted") and vim.api.nvim_buf_get_option(buf, "buftype") == ""
    end

    -- check if a buffer is active
    for i, buf in ipairs(bufs) do
      if (buf == conf.current_buf and should_list(buf)) then
	self.last_active_buf = nil
      end
    end

    -- add buffers to the list
    for i, buf in ipairs(bufs) do
      if (should_list(buf)) then
	local name = vim.fn.fnamemodify(vim.api.nvim_buf_get_name(buf), ":p:t")
	local active = buf == conf.current_buf
	local last_active = buf == self.last_active_buf
	local modified = vim.api.nvim_buf_get_option(buf, "modified")
	local hl = nil

	if (active and modified) then hl = "blue"
	elseif (active) then hl = "gray"
	elseif (last_active) then hl = "color2"
	else hl = "color1" end
	if (modified) then name = name.."+" end

	if (active) then self.last_active_buf = buf end

	table.insert(listed, {
	  text = " "..name.." ",
	  hl = hl,
	  active = active or last_active
	})
      end
    end

    local conf = {
      separator = symbols.right_sep,
      alt_separator = symbols.right_alt_sep,
      alt_sep_hl = self.hl:blend("color3", "color1")
    }
    builder:tabs(conf, listed)
  end)

  self.buffer_info_part = Part:new(function(conf, builder)
    builder:separate(symbols.left_sep, true)
    builder:write("color2_bold", " buffers ")
  end)
end

function DefaultTheme:setup_lines()
  self.main_line = Line:new("color2")
  self.main_line:add_left(self.mode_part)
  self.main_line:add_left(self.name_part)
  self.main_line:add_right(self.type_part)
  self.main_line:add_right(self.pos_part)

  self.help_line = Line:new("color2")
  self.help_line:add_left(self.help_part)
  self.help_line:add_left(self.name_part)

  self.other_line = Line:new("color1")
  self.other_line:add_left(self.name_part)

  self.tab_line = Line:new("none_bg")
  self.tab_line:add_left(self.buffers_part)
  self.tab_line:add_left(self.empty_part)
  self.tab_line:add_right(self.buffer_info_part)

  self:add_status_line(self.main_line)
  self:add_status_line(self.other_line)
  self:add_tab_line(self.tab_line)
end

function DefaultTheme:render_status_line(win)
  local mode = vim.api.nvim_get_mode()
  local mode_name, mode_short = Help:get_mode_name(mode.mode)

  local accent = "gray"
  if (mode_short == "i") then accent = "blue"
  elseif (mode_short == "r") then accent = "green"
  elseif (mode_short == "v") then accent = "orange" end

  local conf = {
    win = win,
    buf = vim.api.nvim_win_get_buf(win),
    hl_set = self.hl,
    mode = mode,
    mode_name = mode_name,
    mode_short = mode_short,
    accent = accent
  }

  for i, line in ipairs(self.config.status_lines) do
    if (line:is_suitable(conf)) then
      return line:render(conf)
    end
  end

  if (vim.api.nvim_buf_get_option(conf.buf, "buftype") == "help") then
    return self.help_line:render(conf)
  elseif (conf.win == vim.api.nvim_get_current_win()) then
    return self.main_line:render(conf)
  end
  return self.other_line:render(conf)
end

function DefaultTheme:render_tab_line(tab)
  local conf = {
    tab = tab,
    current_win = vim.api.nvim_get_current_win(),
    current_buf = vim.api.nvim_get_current_buf(),
    hl_set = self.hl
  }

  for i, line in ipairs(self.config.tab_lines) do
    if (line:is_suitable(conf)) then
      return line:render(conf)
    end
  end
  return self.tab_line:render(conf)
end

return DefaultTheme
