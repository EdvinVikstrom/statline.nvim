local HlSet = {}

---@param namespace  highlight group prefix
function HlSet:new(namespace)
  local self = setmetatable({}, { __index = HlSet })
  self.namespace = namespace
  self.hls = {}
  return self
end

---@param name  highlight group name
---@param blend  generate blending highlights if true
---@param cterm  highlight options
---@param ctermfg  highlight foreground
---@param ctermbg  highlight background
function HlSet:add_hl(name, blend, cterm, ctermfg, ctermbg)
  table.insert(self.hls, {name = name, blend = blend, cterm = cterm, ctermfg = ctermfg, ctermbg = ctermbg})
end

function HlSet:define_all()
  for i1, hl1 in ipairs(self.hls) do
    self:define_hl(hl1.name, hl1.cterm, hl1.ctermfg, hl1.ctermbg)
    if (hl1.blend) then
      for i2, hl2 in ipairs(self.hls) do
	self:define_hl(hl1.name.."_to_"..hl2.name, hl1.cterm, hl1.ctermbg, hl2.ctermbg)
      end
    end
  end
end

---@param name  highlight group name
---@param cterm  highlight options
---@param ctermfg  highlight foreground
---@param ctermbg  highlight background
function HlSet:define_hl(name, cterm, ctermfg, ctermbg)
  if (cterm == nil and ctermfg == nil and ctermbg == nil) then return end
  local opts = ""
  if (cterm) then opts = opts.." cterm="..cterm end
  if (ctermfg) then opts = opts.." ctermfg="..ctermfg end
  if (ctermbg) then opts = opts.." ctermbg="..ctermbg end
  vim.cmd("hi "..self.namespace.."_"..name..opts)
end

---@param hl1  highlight group
---@param hl2  highlight group
---@returns highlight group that blends hl1 and hl2
function HlSet:blend(hl1, hl2)
  return hl1.."_to_"..hl2
end

---@param name  highlight group
---@returns name with namespace as prefix
function HlSet:get(name)
  return self.namespace.."_"..name
end

return HlSet
