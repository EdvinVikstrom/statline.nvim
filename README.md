# Pretty status line for Neovim
Requires Neovim 0.7+

### Example conifg file
```
local StatLine = require("statline")
StatLine:setup({
  enable_tab_line = true
})
```
